 +

[cols=",,",]
|===
|image:destaque-carrossel-fotos_html_7068636e832bf7d5.jpg[image,width=127,height=35] + a|
 +

*Componente*

 +

|*Agência Brasil 2017*
|===

 +

[cols=",",]
|===
|*Projeto* |Agência Brasil 2017
|*Responsáveis* |Jonathan Muribeca, Vinícius Monteiro
|===

 +
 +

[cols="",]
|===
|_*Nome do Componente:* Destaque Carrossel_
|*_Informações de Acesso:_*
a|
 +

[cols=",,",]
|===
|*Ambiente de Desenvolvimento* |*Ambiente de Homologação* |*Ambiente de Produção*
|http://agenciabrasil2017-devel.ebc | | +
|===

 +

[cols=",,",]
|===
|*Perfil* |*Descrição* |*Permissão*
|*_N/A_* |*_N/A_* |*_N/A_*
|===

 +

a|
*_Regras de Negócio:_*

 +

. *O componente será limitado a 4(quatro) destaques*
. *Os destaques serão rotativos em um intervalo de 10 segundos*
. *O crédito do fotografo será o cadastrado na foto*
. *Cada destaque terá seus próprios campos de Título, Mídia, Editoria, Data de publicação e localização*
. *Caso não haja destaque cadastrado, o componente deverá ser ocultado*

 +

a|
*_Campos:_*

 +

[cols=",,,,",]
|===
|*Campo* |*Tipo* |*Tamanho* |*Valores* |*Regras*
|Título |Texto |70 caracteres |N/A |Obrigatório
|Imagem de destaque |Foto |8 MB |N/A |Obrigatório
|Editoria |Lista |N/A a|
Direitos Humanos

Economia,

Educação,

Geral,

Internacional,

Justiça,

Política,

Saúde

|Obrigatório
|Data de publicação |Data/hora |N/A |DD/MM/AAAA HH:MM |Opcional
|Localização |Autocomplete |255 caracteres |N/A |Opcional
|===

 +

a|
Layout no final do documento

 +

|*_Campos de Visualização:_*
a|
*_Cenário: Ao acessar a página de fotos (/fotos) a partir do desktop_*

E visualizo a barra de menus institucional

E visualizo a barra de menus do veículo

E visualizo o componente de carrossel de fotos que mostra a foto, abaixo da foto a data de publicação, seguida pela editoria, fotografo e localização

E eu visualizo o título

Ao posicionar o ponteiro do mouse sobre a foto são exibidos indicadores laterais de navegação que, ao se clicar no da direita, eu visualizo o próximo destaque ou se houver, se clicar no da esquerda, visualizo o destaque anterior se houver

 +

*_Cenário: Ao acessar a página de fotos (/fotos) a partir do dispositivo mobile_*

E visualizo a barra de menus institucional

E visualizo a barra de menus do veículo

E visualizo o componente de carrossel de fotos que mostra a foto, abaixo da foto a data de publicação seguida pela editoria, fotografo e localização

E eu visualizo o título

Ao arrastar a foto para o lado eu visualizo o próximo destaque se houver

|===

image:destaque-carrossel-fotos_html_b5cdb61c0fc451bd.png[image,width=808,height=638]

 +
 +

 +
 +

 +

[cols=",",]
|===
|Empresa Brasil de Comunicação a|
2

 +

|===

 +
