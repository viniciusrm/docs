 +

[cols=",,",]
|===
|image:destaque-materias-especiais_html_7068636e832bf7d5.jpg[image,width=127,height=35] + a|
 +

*Componente*

 +

|*Agência Brasil 2017*
|===

 +

[cols=",",]
|===
|*Projeto* |Agência Brasil 2017
|*Responsáveis* |Jonathan Muribeca, Vinícius Monteiro
|===

 +
 +

[cols="",]
|===
|_*Nome do Componente:* Destaque Fique de olho_
|*_Informações de Acesso:_*
a|
 +

[cols=",,",]
|===
|*Ambiente de Desenvolvimento* |*Ambiente de Homologação* |*Ambiente de Produção*
|http://agenciabrasil2017-devel.ebc | | +
|===

 +

[cols=",,",]
|===
|*Perfil* |*Descrição* |*Permissão*
|*_N/A_* |*_N/A_* |*_N/A_*
|===

 +

a|
*_Regras de Negócio:_*

 +

. *Exibe 4 itens de forma manual para o idioma Português e 2 itens para os demais idiomas*
. *No campo Chapéu, quando não preenchido será exibido a Editoria.*

 +

a|
*_Campos:_*

 +

[cols=",,,,",]
|===
|*Campo* |*Tipo* |*Tamanho* |*valores* |*Regras*
|Título |Texto |70 caracteres |N/A |Obrigatório
|Chapéu |Texto |70 caracteres |N/A |Opcional
|Editoria |Lista |N/A a|
Direitos Humanos

Economia,

Educação,

Geral,

Internacional,

Justiça,

Política,

Saúde

|Obrigatório
|Link |Url |N/A |N/A |Obrigatório
|===

 +

a|
Layout no final do documento

 +

|*_Teste de aceitação:_*
a|
 +

*_Cenário: Visualizar o componente “Destaque Fique de Olho”_*

_Dado que eu esteja visualizando o componente_

_Então visualizo a “chapéu ou editoria”, será um link para a página da editoria_

_E visualizo o “Título”, será um link para a matéria_

 +

 +

|===

 +
 +

 +
 +

*_Mudar o texto matérias especiais para Fique de Olho_*

image:destaque-materias-especiais_html_5c808ccea35f6281.png[image,width=540,height=320]

 +
 +

 +

[cols=",",]
|===
|Empresa Brasil de Comunicação a|
2

 +

|===

 +
