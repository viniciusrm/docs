= História de Usuários - Página últimas notícias
:toc:
:toc-title: Sumário

== Responsáveis: Vinícius Monteiro

[.tabela-ambiente,options=header]
|===

|Ambiente de Desenvolvimento | Ambiente Homologação | Ambiente de Produção
|http://ambiente-do-projeto-devel.ebc | http://ambiente-do-projeto-homol.ebc |http://ambiente-do-projeto-prod.ebc

|===

[[regras-de-negócio]]
== Regras de Negócio

a|
*_Regras de Negócio:_*

 +
. Incluir filtros na página de últimas notícias, e após serem utilizados, o usuário tem a possibilidade de assina, para receber um push notification.
. *Apresentará as 12 últimas notícias de todo o site.*
.. Cada item da lista deverá conter título+link, data de publicação, imagem de destaque quando houver, indicativo de player quando possuir mídia no campo mídia de destaque do conteúdo, editoria e barra de compartilhamento
.. No final da lista será apresentado um botão “Ver mais” ao ser clicado, carregará mais 12 itens na mesma página
. *A imagem de destaque de cada item deverá obedecer ao seguinte critério:*
.. A foto presente no campo “Imagem Destaque” do conteúdo
.. Imagem do campo mídia de destaque (caso a mídia de destaque seja uma imagem)
.. A foto gerada automaticamente a partir do vídeo, pelo campo de “Mídia” do conteúdo caso a mídia seja um vídeo,
.. Não existindo a “Imagem de destaque” ou a “Mídia” como foto ou vídeo, não será exibido a imagem de destaque
. *O componente apresentara data de publicação e título clicável para a página do conteúdo.*
. *Quando houver imagem disponível no componente, ela deverá ser clicável para a página do conteúdo*
. *Quando a notícia for atualizada, ela vai para o topo da lista e exibirá a data de atualização com indicativo de “atualizada”.*

 +

a|
*Campos Back-End:*

 +

[cols=",,,,",]
|===
|Campo |Tipo |Tamanho |Valores |Regras
| + | + | + | + | +
|===

 +

 +

a|
_Layout no final do documento_

 +

|*_Componentes Referenciados:_*
a|
_\levantamento-de-requisitos\componente\barra-menu_

_\levantamento-de-requisitos\componente\barra-compartilhamento_

 +

 +

|*_Teste de Aceitação:_*
a|
*_Cenário: Visualizar a página de últimas_*

_Dado que eu esteja na página de últimas_

_Então visualizo os” Destaques” apresenta 12 notícias de todo o site com imagem de destaque quando houver e indicativo de player quando possuir mídia de destaque no conteúdo, será um link para o conteúdo da matéria_

_E visualizo o "o Ícone de Atualizado junto com a Data e hora de Atualização e Editoria", exibe na ordem da data de atualização_

_E visualizo o "Barra de Compartilhamento"_

_E visualizo o “Título”, será um link para o conteúdo da matéria_

_E visualizo o “Botão Ver Mais”,_ ao ser clicado, carregará mais 12 itens na mesma página

| +
|===


image:ultimas-noticias_html_66111a02201825a6.jpg[image,width=674,height=947]
