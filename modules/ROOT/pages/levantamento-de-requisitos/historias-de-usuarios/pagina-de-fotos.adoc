[cols=",,",]
|===
|image:pagina-de-fotos_html_7068636e832bf7d5.jpg[image,width=126,height=35] + a|
 +

*História de Usuários*

 +

|*Agência* *Brasil 2017*
|===

 +

[cols=",",]
|===
|*Projeto* a|
* Agência Brasil 2017

|*Responsáveis* a|
* Jonathan Muribeca, Vinícius Monteiro

|===

[cols="",]
|===
|*_PÁGINA DE FOTOS_*
|*_Informações de Acesso:_*
a|
[cols=",,",]
|===
|*Ambiente de Desenvolvimento* |*Ambiente de Homologação* |*Ambiente de Produção*
|_http://agenciabrasil2017-devel.ebc/_ | | +
|===

 +

[cols=",,",]
|===
|*Perfil* |*Descrição* |*Permissão*
|*_N/A_* |*_N/A_* |*_N/A_*
|===

 +

a|
*_Regras de Negócio:_*

 +

. O título do componente será fixo e não editável (Fotos Recentes)
.. Inicialmente exibirá abaixo do destaque as últimas 12 Galerias de fotos publicadas ordenadas da mais recente para mais antiga. Ao final da lista será exibido o paginador para as fotos anteriores
. Ao clicar na galeria, abrirá em uma nova janela a página da foto
. O crédito da foto seguirá o formato <nome do fotógrafo>/Agência Brasil
. Exibe o campo de busca
.. O termo da busca deverá levar em consideração o título, descrição, localização e tags da foto
.. Será possível filtrar o resultado por: fotógrafo, editoria e data de publicação (início <-> fim)

 +

a|
_Layout no final do documento_

 +

|*_Componentes Referenciados:_*
a|
_\levantamento-de-requisitos\componente\barra-menu-veiculo_

_\levantamento-de-requisitos\componente\barra-compartilhamento_

_\levantamento-de-requisitos\componente\destaque-colecao-foto_

 +

|*_Teste de Aceitação:_*
a|
*_Cenário: Visualizar a Página de Fotos_*

_Dado que eu esteja na Página de Fotos_

_Então visualizo a "Barra de Menu do Veiculo"_

_E visualizo_ _o componente de destaque com informações de data de publicação, editoria, crédito e título_

_E visualizo_ _as_ _"__Galerias de_ _Fotos__",__exibindo o crédito do fotografo_ _e com no máximo de_ _12_ _fotos exibidas_ _e o botão “Ver mais” que carrega mais 12 galerias de fotos na mesma página_

_E visualizo a "Barra de publicidade"_

_E visualizo o componente de coleção de fotos em destaques_

_E visualizo o Rodapé institucional_

 +

| +
|===



image:pagina-de-fotos_html_427f7d6457364e78.jpg[image,width=336,height=854]
