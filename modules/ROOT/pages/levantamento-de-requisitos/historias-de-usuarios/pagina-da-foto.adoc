[cols=",,",]
|===
|image:pagina-da-foto_html_7068636e832bf7d5.jpg[image,width=126,height=35] + a|
 +

*História de Usuários*

 +

|*Agência* *Brasil 2017*
|===

 +

[cols=",",]
|===
|*Projeto* a|
* Agência Brasil 2017

|*Responsáveis* a|
* Jonathan Muribeca, Vinícius Monteiro

|===

*_PÁGINA DA FOTO_*

*_Informações de Acesso:_*

[cols=",,",]
|===
|*Ambiente de Desenvolvimento* |*Ambiente de Homologação* |*Ambiente de Produção*
|_http://agenciabrasil2017-devel.ebc/_ | | +
|===

 +

[cols=",,",]
|===
|*Perfil* |*Descrição* |*Permissão*
|*_N/A_* |*_N/A_* |*_N/A_*
|===

 +

*_Regras de Negócio:_*

 +

. *O Título exibido na página será sempre o título da galeria*
. *Abaixo do título será exibida a legenda da Foto*
. *Quando o fotógrafo fizer upload de uma foto, os metadados no padrão IPTC da foto serão utilizados para autopreencher os dados nos campos correspondentes do formulário da foto.*
. *Deverá existir mecanismo que iniba upload de foto em duplicidade*
. *Somente usuários autenticados na Central de conteúdo poderão baixar a foto original.*
. *O atributo Alt da foto deve ser preenchido automaticamente com o valor da Legenda da Foto*
. *O* ** *atributo* ** *Title* *da foto* *deve ser preenchido* *no padrão:* Nome do fotografo / Agência Brasil
. *Os dados IPTC da Foto original não devem ser alterados.*
. *O botão de download deverá prover duas opções para formato de resolução da foto para download:*
.. Formato web: 1024x768
.. Original: 5184 X 3456
. *Abaixo da foto da página será exibida a legenda no seguinte formato:*
+
<Localidade>:
+
<nome do Fotógrafo>/Agência Brasil
+
Ex.: Brasília
+
Marcello Casal / Agência Brasil
. *Fotos de Parceiros não poderão ser baixadas.*
. *Fotos de Parceiros não serão exibidas na lista de fotos.*
. *Haverá barra de compartilhamento.*
. *Após o componente da foto, haverá o componente de fotos (Outras fotos da galeria).*
.. Cada galeria conterá no máximo 20 fotos

 +

[start=15]
. *Na barra de ferramenta abaixo da foto deve exibir duas setas uma para esquerda(<-) e outra para a direita(->), quando acionada pelo usuário através do mouse, o sistema deverá exibir outra foto, inclusive alterando o crédito da foto e localização, título e legenda caso seja diferente.*

 +

 +

 +

 +

*Campos Back-End da Galeria:*

 +

Campo

Tipo

Tamanho

Valores

Regras

Título da Galeria

Texto

70 caracteres

N/A

Obrigatório

Editoria

Lista

N/A

Economia,

Educação,

Saúde,

Geral,

Internacional,

Política

Obrigatório

Data de Atualização da galeria

Data/hora

N/A

N/A

Opcional

 +

 +

 +

 +

 +

 +

*Campos Back-End da Foto:*

Mídia

Foto

8 MB

N/A

Obrigatório

Legenda da Foto

Texto

255 caracteres

N/A

Obrigatório

Autor

Taxonomia

N/A

N/A

*Obrigatório*

Autocomplete (conforme forem cadastradas no site)

Direitos reservados

Boolean

N/A

 +

Opcional

Localização

Taxonomia

N/A

N/A

*Obrigatório*

Autocomplete (conforme forem cadastradas no site)

Editoria

Lista

N/A

Direitos Humanos

Economia,

Educação,

Geral,

Internacional,

Justiça,

Política,

Saúde

Obrigatório

Tags

Texto

N/A

N/A

*Obrigatório*

Autocomplete

 +

 +

 +

_Layout no final do documento_

 +

*_Componentes Referenciados:_*

_\levantamento-de-requisitos\componente\barra-menu_

 +

 +

*_Teste de Aceitação:_*

*_Cenário: Visualizar a Página de Foto_*

_Dado que eu esteja na Página de Foto_

_Então visualizo a "Barra de Menu"_

_E visualizo_ _a_ _"__Foto destacada__"_ _com_ _crédito do fotográfo, localização, contador de fotos, botão de download, botão de maximimar foto, setas para navegar entre as fotos_

_E visualizo_ _a_ _"__Editoria__” que será um_ _link para a página conténdo as matérias daquela editoria_

_E visualizo_ _o_ _"__Título__",_ _com o máximo de 70 caracteres_

_E visualizo_ _a_ _"__Legenda__",_ _com o máximo de_ _255_ _caracteres_

_E visualizo_ _o_ _"__Publicado em__",_ _data de publicação da matéria_

_E visualizo_ _o_ _"__Atualiz__a__do em__",_ _data de atualização da matéria_

_E visualizo_ _a_ _"__Tag__",_ _separado por vírgulas e em caixa alta_

_E visualizo_ _as_ _"__Fotos__",__exibindo o crédito do fotografo_ _e com no máximo de 20 fotos exibidas_

_E visualizo_ _a_ _"__Barra de publicidade__"_

_E visualizo o “Rodapé institucional”_



Modelo Foto Destacada Horizontal

image:pagina-da-foto_html_acd9ecd695d8bb8.jpg[image,width=520,height=914]



Modelo Foto Destacada Vertical

image:pagina-da-foto_html_1205da1a6e4b8972.jpg[image,width=520,height=924]
