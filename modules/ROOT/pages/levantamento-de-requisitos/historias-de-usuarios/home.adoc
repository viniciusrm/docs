= História de Usuários - Página Home
:toc:
:toc-title: Sumário

== Responsáveis: Vinícius Monteiro

[.tabela-ambiente,options=header]
|===

|Ambiente de Desenvolvimento | Ambiente Homologação | Ambiente de Produção
|http://ambiente-do-projeto-devel.ebc | http://ambiente-do-projeto-homol.ebc |http://ambiente-do-projeto-prod.ebc

|===

[[regras-de-negócio]]
== Regras de Negócio

1. Barra institucional e o componente Sobre a EBC deve ser exibido em todas as páginas.
2. Breaking news deve ser exibido de uma margem a outra como um texto.

[[campos-de-back-end]]
== Campos de Back-End

[options=header]
|===

| Campo | Tipo | Tamanho | Valores | Regras

|===


[[componentes-referenciados]]
== Componentes Referenciados

* link:/levantamento-de-requisitos/componente/cabecalho.adoc[Cabeçalho]
* link:/levantamento-de-requisitos/componente/menu.adoc[Menu]
//* link:/levantamento-de-requisitos/componente/banner-slider.adoc[Banner Slider]
* link:/levantamento-de-requisitos/componente/rodape.adoc[Rodapé]
* link:/levantamento-de-requisitos/componente/barra-menu-veiculo.adoc[Barra Menu]
* link:/levantamento-de-requisitos/componente/barra-compartilhamento.adoc[Barra Compartilhamento]
* link:/levantamento-de-requisitos/componente/breaking-news.adoc[Breaking News]
* link:/levantamento-de-requisitos/componente/manchete.adoc[Manchete]
* link:/levantamento-de-requisitos/componente/destaque-principal.adoc[Destaque Principal]
* link:/levantamento-de-requisitos/componente/ultimas-noticias-home.adoc[Destaque últimas notícias home]
* link:/levantamento-de-requisitos/componente/destaque-especiais.adoc[Destaque Especiais]
* link:/levantamento-de-requisitos/componente/destaque-secundario.adoc[Destaque Secundário]
* link:/levantamento-de-requisitos/componente/destaque-fique-de-olho.adoc[Destaque fique de olho]
* link:/levantamento-de-requisitos/componente/destaques-materias-especiais.adoc[Destaque matérias especiais]
* link:/levantamento-de-requisitos/componente/pagina-galeria-midia.adoc[Galeria de Mídia]

[[teste-de-aceitação]]
== Teste de Aceitação

=== Cenário: Visualizar a home

Dado que eu esteja na Home

Então visualizo a "Barra de Menu"

E visualizo o "__Componente Breaking news__" que será uma_ _faixa com um título de uma margem a outra do site_

E visualizo o "Componente_ _Manchete__" que será uma imagem com um chapéu ou linha fina, um título, editoria, até 4 fotos menores e até 3 conteúdos relacionados

E visualizo o "Destaque Secundário" que será uma imagem com uma linha fina, um título, editoria e link de compartilhar

E visualizo o "__Destaque mais lidas__"_ _com 4 matérias_ _as mais lidas do dia__, contendo os campos de editoria e título

E visualizo o "__Destaque fique de olho__"_ _com 4 matérias, contendo os campos de editoria e título

E visualizo o “Componente de últimas notícias” com no máximo 7 notícias e com os campos, título e editoria

E visualizo o “Banner Master”_ _de publicidade

E visualizo o "__Destaque especiais__"_ _com uma foto, uma descrição e um botão com link

E visualizo o "Destaque Secundário” que será uma imagem com_ _uma editoria,_ __ _t__ítulo,_ _linha fina e conteúdos relacionados no máximo_ _12_ __ _destaques

E visualizo o "__Barra de Publicidade__"




E visualizo o "__Destaque galeria de fotos home__"_ _com 4 fotos, uma grande, uma média e duas fotos pequenas, contendo os campos de editoria e legenda da foto

E visualizo o "__Destaque últimas por editoria__"_ _com no máximo 7 destaques, contendo editoria, barra de compartilhamento e título


=== Cenário: Visualizar Últimas Notícias

_Dado que esteja na Home_

_Quando clico no item Últimas Notícias do Menu_

_Então sou redirecionado para página de últimas notícias cadastrado em todo o site_


=== Cenário: Visualizar Fotos

_Dado que esteja na Home_

_Quando clico no item Fotos do Menu_

_Então sou redirecionado para página de Fotos_


=== Cenário: Visualizar Especiais

_Dado que esteja na Home_

_Quando clico no item Especiais do Menu_

_Então sou redirecionado para a página dos especiais_


=== Cenário: Visualizar ìcones de idiomas

_Dado que esteja na Home_

_Quando clico no item ícones de idiomas do Menu_

_Então sou redirecionado para a home relativo ao idioma_


=== Cenário: Visualizar barra de redes sociais

_Dado que esteja na Home_

_Quando clico no item barra de redes sociais do Menu_

_Então sou redirecionado para página relativo a cada rede social da agência Brasil_


=== Cenário: Realizar uma Busca

_Dado que esteja na Home_

_Quando clico na Busca (lupa) do Menu_

_Então é disponibilizado um campo para digitar o texto do que deseja pesquisar_


=== Cenário: Visualizar Editorias

_Dado que esteja na Home_

_Quando clico no item editoria do Menu_

_Então sou redirecionado para a página de cada editoria (página onde exibe as matérias relativo o cada editoria)_


=== Cenário: Visualizar Breaking News

_Dado que esteja na Home_

_Quando clico no item Breaking News (título)_

_Então sou redirecionado para a página da Matéria destacada_


=== Cenário: Visualizar um destaque Principal

_Dado que esteja na Home_

_Quando clico em um destaque Principal_

_Então sou redirecionado para a página do conteúdo interna e visualizo a página do conteúdo e/ou assisto ao vídeo caso possua_


=== Cenário: Visualizar Componente de últimas notícias

_Dado que esteja na Home_

_Quando clico no em algum item relacionado no componente de últimas notícias_

_Então sou redirecionado para a página do conteúdo interna e visualizo a página de conteúdo e/ou assisto ao vídeo caso possua_


=== Cenário: Visualizar Banner Master

_Dado que esteja na Home_

_Quando clico no banner master_

_Então sou redirecionado para a página de Publicidade, seja ela do próprio site ou site externo_


=== Cenário: Visualizar destaque Especial

_Dado que esteja na Home_

_Quando clico no botão (link)_

_Então sou redirecionado para a página do conteúdo especial_


=== Cenário: Visualizar destaque Secundário

_Dado que esteja na Home_

_Quando clico em um destaque Secundário_

_Então sou redirecionado para a página do conteúdo interna e visualizo a página do conteúdo e/ou assisto ao vídeo caso possua_


=== Cenário: Visualizar barra de publicidade

_Dado que esteja na Home_

_Quando clico em algum conteúdo de destaque_

_Então sou redirecionado para a página de Publicidade, seja ela do próprio site ou site externo_


=== Cenário: Visualizar conteúdo do destaque mais lidas

_Dado que esteja na Home_

_Quando clico em algum conteúdo de destaque_

_Então sou redirecionado para a página de conteúdo interna e visualizo a página do conteúdo e/ou assisto ao vídeo_


=== Cenário: Visualizar conteúdo do destaque fique de olho

_Dado que esteja na Home_

_Quando clico em algum conteúdo de destaque_

_Então sou redirecionado para a página de conteúdo interna e visualizo a página do conteúdo e/ou assisto ao vídeo_


=== Cenário: Visualizar destaque de galeria de fotos da home

_Dado que esteja na Home_

_Quando clico em um destaque_

_Então sou redirecionado para a página da foto, que também contém as demais fotos relativo a essa galeria_

[[layout]]
== Layout

image::../../servico-de-apoio/layouts/novo/Home-novo.png[]
