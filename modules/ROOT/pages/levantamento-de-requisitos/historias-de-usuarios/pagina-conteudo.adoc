= História de Usuários - Página conteúdo
:toc:
:toc-title: Sumário

== Responsáveis: Vinícius Monteiro

[.tabela-ambiente,options=header]
|===

|Ambiente de Desenvolvimento | Ambiente Homologação | Ambiente de Produção
|http://ambiente-do-projeto-devel.ebc | http://ambiente-do-projeto-homol.ebc |http://ambiente-do-projeto-prod.ebc

|===

[[regras-de-negócio]]
== Regras de Negócio

. *O idioma da interface administrativa será definida de acordo com o perfil do usuário*
. .. Rastreabilidade dos conteúdos disponibilizados por outros veículos. Exemplo: Qual tipo de informação é mais consumida pela (UOL), informação sobre política ou, no caso do Terra, nas quartas-feiras é consumida mais informação sobre economia.
. *O conteúdo deverá pertencer a um idioma (Português Brasileiro, Inglês ou espanhol)*
. *O sistema deverá prover de forma automática uma URL para a versão AMP (Accelerated Mobile Pages) de cada página de conteúdo*
. *Quando um novo conteúdo for publicado, os caches internos, externos e da CDN deverão ser invalidados automaticamente para a página de últimas notícias*
. *Quando um conteúdo publicado for atualizado, deverá ocorrer invalidação dos caches internos, externos e de CDN automaticamente na url do conteúdo e da página de últimas notícias.*
. *O sistema deverá manter cadastro dos parceiros com dados de:*
.. Nome
.. Logo (Imagem da marca com resolução de X x Y)
.. Link
. *Será exibido o componente “Saiba mais” logo após as informações de edição e tradução e antes das tags*
. *Os usuários habilitados à criação e edição de conteúdo só poderão criar ou editar conteúdos de idiomas permitidos em seu perfil (poderá ser mais de um idioma permitido)*
. *Só será inserido uma imagem ou vídeo ou áudio no campo mídia de destaque por conteúdo.*
. *Possibilidade de inserir até 3 conteúdos relacionados (Saiba Mais)*
. *Banner master à direita do conteúdo*
. *Imagem adicionada no corpo do conteúdo deve possibilitar alinhamento à esquerda, direita e centralizada, com tamanhos de 3 colunas, 4 colunas, 6 colunas e 8 colunas (Coluna cheia)*
. *A legenda da imagem no corpo do conteúdo deve ser editável*
.. Deverá obedecer o seguinte formato: <Legenda> - <autor>
... Ex.: Aprovação da lei a tempo da eleição de 2018 - Marcelo Camargo
. *O Vídeo no corpo do conteúdo deve ocupar sempre a coluna cheia*
. *Ao final do conteúdo, o campo TAGS exibe os valores em caixa alta e separados por vírgula(,)*
. *Ao final do conteúdo de parceiros um texto de direitos reservados é exibido.*
. *Ao final do conteúdo apresentará o componente de “Últimas notícias” com as 6 últimas notícias do site e o botão “Ver mais” que ao ser clicado, abre a página de Últimas notícias*
. *Cada editor só poderá criar e editar conteúdos no idioma a que sua conta de usuário esteja habilitada*

 +

*Campos Back-End:*

 +

*Campo*

*Tipo*

*Tamanho*

*Valores*

*Regras*

Título

Texto

70 caracteres

N/A

Obrigatório

Linha fina

Texto

70 caracteres

N/A

Opcional

Data de Publicação

Data/hora

N/A

DD/MM/AAAA HH:MM

Opcional –

* Se não preenchido manualmente, preenche com data e hora atual e publica caso o usuário marque a opção de “publicado”
*  Em caso de data passada *não* auto publicar.
* Pode ser preenchido com data futura (Agendado para publicação automática).
* É ocultado na edição de conteúdo já publicado.

Data de Atualização

Data/hora

N/A

DD/MM/AAAA HH:MM

Opcional /Manual

(Oculto. Só aparece na edição de conteúdo já publicado)

Por

Texto

255 caracteres

N/A

Obrigatório (Normalmente preenchido com o nome do repórter – preenchimento manual)

Localização

Autocomplete

255 caracteres

N/A

Obrigatório

Parceiro

Lista

N/A

N/A

Opcional (Exibirá logo como link para a página do parceiro)

Corpo com sumário

Texto

N/A

Imagem, Vídeos, Urls, embed do youtube, áudio e texto

Obrigatório (Se o sumário não for preenchido, deve ser considerado como valor o primeiro parágrafo do corpo)

Mídia

Foto/Vídeo/áudio

Foto 8MB / Vídeo 2GB / Áudio 100MB

N/A

Opcional

Imagem Destaque

Imagem

8MB

N/A

Opcional

Conteúdos Relacionados(Saiba Mais)

Link

N/A

N/A

Opcional (até 3 itens) Campo com função de autocomplete com base nos títulos dos conteúdos publicados do site.

Tags

Texto

N/A

N/A

Opcional – Autocomplete (Lista de termos separados por vírgula)

Idioma

Lista

N/A

Português / Inglês / Espanhol

Obrigatório (Valor padrão autopreenchido pelo contexto do usuário)

Editoria

Lista

N/A

Direitos Humanos

Economia,

Educação,

Geral,

Internacional,

Justiça,

Política,

Saúde

Obrigatório

Edição

Texto

255

N/A

Opcional

Autopreenchido com o nome do usuário que criou o conteúdo, mas deve ser editável manualmente

Tradução

Texto

255

N/A

Opcional

 +

 +

_Layout no final do documento_

 +

*_Componentes Referenciados:_*

_\levantamento-de-requisitos\componente\barra-menu-veiculo_

_\levantamento-de-requisitos\componente\barra-compartilhamento_

 +

*_Teste de Aceitação:_*

*_Sucesso:_*

 +

*_Cenário: Visualizar a página de conteúdo_*

_Dado que eu esteja na página do conteúdo_

_Então visualizo” Vídeo destacado” com o player do vídeo se houver_

_E visualizo “Editoria” (na cor da editoria)_

_E visualizo “Título”, com máximo de 70 caracteres_

_E visualizo a "Linha fina" na cor da editoria_

_E visualizo o "Publicado em"_

_E visualizo o "Atualizado em"_

_E visualizo o "Por"_

_E visualizo o "Corpo do Texto" – que pode conter: Imagens, Citação, Vídeo, Áudio, Link e Banner Master"_

_E visualizo o "Banner Master"_

_E visualizo o "Parceiro”_

_E visualizo o "Barra de Compartilhamento”_

_E visualizo o “Componente Saiba Mais”_

_E visualizo o "Edição"_

_E visualizo o "Tradução"_

_E visualizo o "Tags"_

_E visualizo o "Publicidade"_

_E visualizo o "Componente de Ultimas”_

_E visualizo o “Botão Ver Mais”_

 +

*_Cenário: Visualizar o vídeo destacado_*

_Dado que esteja na página de conteúdo_

_Quando clico no player do vídeo_

_Então ele será exibido_

 +

*_Cenário: Visualizar algum conteúdo do Componente de Últimas_*

_Dado que esteja na página de conteúdo_

_Quando clico em algum item do componente de últimas_

_Então sou redirecionado para a página do conteúdo selecionado_

 +

*_Cenário: Visualizar o link saiba mais_*

_Dado que esteja na página de conteúdo_

_Quando clico no link do saiba mais_

_Então sou redirecionado para a página do conteúdo_

 +

*_Cenário: Compartilhar conteúdo nas redes sociais_*

_Dado que esteja na página de conteúdo_

_Quando clico em algum ícone de compartilhamento de alguma rede social disponível_

_Então o site executa uma funcionalidade que permite ao usuário compartilhar o conteúdo_



image:pagina-conteudo_html_da540b6b26bbda8.jpg[image,width=245,height=957]
