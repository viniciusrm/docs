= Manual da Agência Brasil
vinicius.monteiro <vinicius.monteiro@ebc.com.br>
:description: Agência Brasil
:keywords: manual, usuário, sistemas
:toc:
:toc-title: Sumário


.Link do sistema:
http://agenciabrasil.ebc.com.br

= 1 Acesso

Para logar no site da Agência Brasil é necessário que o usuário esteja cadastrado com um perfil válido e tenha Login e Senha de rede. O acesso será via Mosaico;

== 1.1 Endereço
http://www.mosaico.ebc.com.br/[www.mosaico.ebc.com.br]
image:img/login.jpg[image,width=738,height=588]


= 2 Perfis

== 2.1 Administrador de usuários

Este perfil tem acesso ao menu Pessoas, que possibilita ao administrador adicionar ou excluir usuários, bem como as devidas permissões no site;

- Administra usuários
- Visualiza perfis de usuários

== 2.2 Editor chefe

- Publica e edita qualquer conteúdo
- Cria e edita destaques
- Despublica e exclui qualquer conteúdo
- Tem acesso às revisões de conteúdo
- Cria Página básica
- Edita Urls alternativas
- Cria, edita e exclui tags
- Cria, edita, publica e exclui fotos
- Agendamento

== 2.3 Repórter

- Somente Cria Conteúdo
- Não publica conteúdo
- Não despublica conteúdo
- Não apaga conteúdo
- Não edita conteúdo
- Não agenda publicação


== 2.4 Editor

- Publica e edita qualquer conteúdo
- Cria e edita destaques
- Não despublica conteúdo
- Não apaga conteúdo
- Não tem acesso às revisões de conteúdo

== 2.5 Fotógrafo

- Cria, edita e publica galeria de fotos
- Cria fotos
- Não despublica Galeria de fotos
- Não exclui Galeria de fotos
- Não despublica ou exclui vídeos


= 3 Funções do Sistema
== 3.1 Página Inicial
Exibe todos os menus e conteúdos adicionados à página inicial.

image:img/pagina-inicial.png[image,width=748,height=872]

== 3.2 Encontrar Conteúdo

Para Encontrar um conteúdo é necessário acessar o *Menu >> Encontrar conteúdo*

image:img/encontrar-conteudo.png[image,width=792,height=501]

Ao clicar no menu *“Encontrar conteúdo”* o sistema exibe uma listagem com os conteúdos do tipo (Conteúdo e Página Básica) já cadastrados no sistema,
caso não exista nenhum conteúdo cadastrado será exibida uma mensagem “nenhum conteúdo cadastrado”.

Será apresentado os seguintes campos:

.. *Título:* Campo texto para preenchimento com o Título do conteúdo que o usuário deseja pesquisar;

.. *Tipo:* Qual o tipo de conteúdo (Conteúdo e Página Básica) que o usuário deseja pesquisar;

.. *Autor:* Qual o usuário que cadastrou o conteúdo no sistema;

.. *Publicado:* Opção “sim/não”, para o usuário selecionar se deseja pesquisar conteúdos publicados ou não publicados;

.. *Idioma:* Qual idioma (português, inglês, espanhol e Qualquer) se deseja pesquisar;

.. *Tags:* Pesquisar matérias contendo as tags informada pelo usuário;

.. *Operação:* Caso o usuário deseje “*Editar”* um conteúdo o usuário clica no link
“*Editar*” e o sistema direciona o usuário para a página de “Edição”(alteração) que já vem com os campos preenchidos para alteração ou caso o usuário deseje
“*Apagar*" um conteúdo o usuário clica no link “*Apagar*”;

Após o preenchimento do campo o usuário pode clicar no botão “**Aplicar**” para o sistema processar a pesquisa com os campos de parâmetros acima citados informados
ou clicar no botão “**Reiniciar**” aonde o programa limpa os campos para uma nova pesquisa.

== 3.3 Adicionar Conteúdo (Reporter, Editor, Editor Chefe)

image:img/adicionar-conteudo.png[image,width=670,height=860]

Para adicionar um conteúdo é necessário acessar o *Menu >> Adicionar Conteúdo >> Conteúdo.*

Você deve informar obrigatoriamente os campos marcados com "*" e opcionalmente informar os outros campos.

.. *Idioma:* Qual o idioma que a página será cadastrada no sistema;

.. *Título:* Campo descrição livre com o máximo de 70 caracteres;

.. *Linha fina:* Campo Descrição livre com o máximo de 70 caracteres;

.. *Resumo:* Esse campo será utilizado para os meios de motor de busca (google e outros);

.. *Corpo:* Campo livre para o usuário descrever texto, inserir links, texto em itálico, negrito, citações, áudio, embed do youtube, imagens, vídeos, galeria de fotos;

.. *Parceiros:* Listagem contendo o nome dos Parceiros;

.. *Por:* Preenchido com o nome do Repórter;

.. *Edição:* Preenchido com o nome do Editor;

.. *Tradutor:* Preenchido com o nome do Tradutor;

.. *Localização:* Preenchido com a localização do conteúdo;

.. *Editoria:* Lista com as editorias a qual o conteúdo pertence (Economia,Educação,Geral,Internacional,Política,Saúde);

.. *Conteúdo Relacionado:* Permitido incluir somente 3 conteúdo relacionado (Somente permite relacionar Matérias da Agência Brasil);

.. *Tags:* Tags relacionada ao conteúdo para facilitar a busca nos motores de busca;

.. *Imagem:* Deve ser arrastada da biblioteca de Mídias. Caso não seja selecionada, nenhuma imagem será apresentada no destaque;

.. *Mídia de Destaque:* Deve ser arrastada da biblioteca de Mídias. Mídias permitidas( Áudio, Vídeo).Caso não seja selecionada, nenhuma mídia será apresentada;

.. *Data de Publicação:* Data e hora a qual o conteúdo foi publicado.

.. *Data de Atualização:* Data e hora a qual o conteúdo foi atualizado.
.. *SEO em tempo real para conteúdo:* Analisa o conteúdo em tempo real otimizando as matérias para sites de buscas.
... *SEO:* Nessa área é possível definir as informações de como irá aparecer nos sites de busca e a palavra-chave. +
    Ao clicar em Editar amostra, é possível definir o Título, Slug (url da página), e Meta-descrição. Um detalhe importante é a barra abaixo do Título e Meta-descrição, elas são uma forma de avaliar cada item (ele varia entre a cor laranja e verde). +
    Abaixo da amostra é necessário definir a palavra-chave, a partir desta palavra o Yoast SEO irá mostrar dicas para otimizar o conteúdo da página, para os sites de bucas. Esssas melhorias são indicadas pelos círculos laranja e vermelho, os itens marcados pelo círculo verde são itens com boa avaliação.

.. *Informação de Revisão:* Contém informações sobre as revisões do conteúdo.Pode ser informado mensagem de registro de alterações e se deseja criar nova revisão sem moderação ou criar nova revisão e moderar.A moderação significa que a nova versão não é visível publicamente até que seja aprovada por alguém com as permissões apropriadas.

.. *Opção de Agendamento*: O usuário pode optar por agendar a publicação do conteúdo para uma data futura.

... *Obs*: Após o usuário agendar um conteúdo o mesmo pode ser consultado no menu *Encontrar Conteúdo >> Aba Agendado*:

image:img/aba-agendado.png[image,width=765,height=158]

.. *Opção de* *Publicação*: O usuário pode optar por (publicado ou despublicado).

Após o preenchimento dos campos o usuário deve clicar no botão “**Salvar**”.

== 3.5 Página de Fotos (Fotógrafo)

Para adicionar uma Página de Fotos é necessário acessar o *Menu* >> *Adicionar Conteúdo* >> *Página de Fotos*.
Em seguida o sistema exibe a tela abaixo:

image:img/pagina-fotos.png[image,width=765,height=242]

O usuário deve clicar no botão “*Adicionar Arquivos*”, para selecionar as imagens e clicar no botão “*Continuar*”.
Em seguida o sistema exibe a tela abaixo:

image:img/pagina-inserir-fotos.png[image,width=765,height=590]

Você deve informar obrigatoriamente os campos marcados com “*” e opcionalmente informar os outros campos.

Nessa parte para cada foto inserida na página de fotos o usuário deve obrigatoriamente os campos marcados com “*” e opcionalmente informar os outros campos.

**Obs:** Caso a foto já possua metadados (IPTC) os campos são preenchidos automaticamente pelo sistema.

.. *Titulo:* Título da foto;

.. *Imagem:* Imagem da foto;

.. *Legenda:* legenda da foto (contém o campo descrição) do IPTC;

.. *Editoria:* Qual a editoria o destaque pertence (Direitos Humanos,Economia, Educação, Geral, Internacional, Justiça, Política, Saúde);

.. *Créditos:* Nome do fotógrafo;

.. *Tags:* Tags relacionada ao conteúdo para falicitar a busca nos motores de busca;

.. *Localização:* Local da Foto;

.. *Direitos reservados:* Caso seja marcado a foto é protegida com direitos autorais.

=== Inclusão de Fotos

Para efetivar a inclusão da foto no sistema o **Editor chefe** deve **Publicar a Foto(s)** para essa ação,

O usuário seleciona no menu **“Encontrar Conteúdo”** em seguida clica na aba **Fotos Pendentes**

Em seguida o sistema exibe a tela abaixo:

image:img/foto-pendente.png[image,width=765,height=386]

O usuário deve selecionar a foto ou fotos, e ainda tem a opção de selecionar todas as pendentes para Publicação e clicar no botão **Publicar Foto(s)**. +
O sistema exibe uma janela para confirmar a publicação das fotos selecionadas.
Caso seja selecionado o botão **OK**, o sistema processa e inclui as fotos no sistema e exibe a mensagem "X foto(s) processada(s)"
Caso seja selecionado o botão **Cancelar**, o sistema fecha a janela.
Caso o usuário **não deseje** publicar a foto(s), deve clicar no botão **Excluir Foto(s)**.

=== Pesquisa de Fotos

Após a publicação das fotos feita pelo editor chefe, é possível pesquisar as imagens na aba **Atoms** no menu **Encontrar Conteúdo**

Em seguida o sistema exibe a tela abaixo:

image:img/busca-atom.png[image,width=765,height=386]

O usuário deve obrigatoriamente preencher os campos marcados com “*” e opcionalmente informar os outros campos.

.. *Titulo:* Título da foto;

.. *Editores:* Nome do editor que publicou a foto;

.. *Autores:* Nome do Autor;

.. *Tipos:* Tipo de atom (arquivos);

.. *Provedores/Serviços:* Título da foto;

.. *Tags:* Tags contidas na foto;

Após preencher o(s) campo(s) desejado, o usuário deve clicar no botão *“Aplicar”* para efetuar a pesquisa ou no botão *“Reiniciar”* para limpar os campos de filtros e cancelar a pesquisa.

O sistema exibe uma listagem com as fotos encontradas de acordo com os filtros selecionados.

=== Alteração dos dados da foto(s)

Para alterar uma foto, o usuário deve clicar no link *Editar*, o sistema deve exibir a tela abaixo:

image:img/alteracao-atom-fotos.png[image,width=765,height=386]

O usuário deve obrigatoriamente preencher os campos marcados com “*” e opcionalmente informar os outros campos.

.. *Titulo:** Título da foto;

.. *Ações disponíveis:** Se outros usuários pode (Buscar,Editar,Ver ou Apagar) a foto;

.. *Thumbnail:** Inserir a imagem que será exibida na biblioteca de mídias;

.. *Legenda:** legenda da foto (contém o campo descrição) do IPTC;

.. *Editoria:** Qual a editoria o destaque pertence (Economia, Educação, Geral, Internacional, Política, Saúde);

.. *Créditos:** Nome do fotógrafo;

.. *Tags:** Tags relacionada ao conteúdo para facilitar a busca nos motores de busca;

.. *Localização:** Local da Foto;

.. *Direitos reservados:* Caso seja marcado a foto é protegida com direitos autorais.

Após alterar o(s) campo(s) desejado, o usuário deve clicar no botão *“Finalizar”* para aplicar as alterações ou no botão *“Cancelar”* para cancelar a alteração e fechar a janela.

*Para Remover uma foto:* O usuário deve clicar no botão .

=== Busca página de fotos (Todos os perfis)

Para realizar uma busca na página de fotos é necessário acessar o *Menu* >> *Página de Fotos*.Em seguida o sistema exibe a tela abaixo:

image:img/pagina-busca-fotos.png[image,width=765,height=396] +

O usuário deve informar os seguintes campos para filtrar a busca:

.. *Termo para buscar as* *fotos:* Leva em consideração o Título e o Corpo(legenda da foto);

* Após o preenchimento dos campos o usuário deve clicar no botão *“Buscar”*.
* O sistema redireciona o usuário para a página das fotos que estão de acordo com o critério da busca.Para o usuário visualizar mais fotos deve clicar no botão *“Ver Mais”.*

* Ao clicar em uma foto o usuário é redirecionado pelo sistema para a “*Página de Fotos”* que contém a foto e os dados da foto que foi selecionada(clicada).

O sistema exibe a página abaixo:

image:img/pagina-interna-foto.png[image,width=765,height=396] +

* Caso o usuário queira ver a foto em tamanho maior deve clicar no ícone de grade que fica no lado direito (canto superior).
* Caso o usuário clique no botão *Ver Fotos* o sistema redireciona o usuário para a página de fotos.
* Caso o usuário queira baixar a foto para seu computador deve clicar no botão *Download Web* ou *Download Original*

== 3.6 Coleção de Fotos Home (Fotógrafo)

image:img/colecao-fotos-home.png[image,width=765,height=165] +

Para alterar a galeria de fotos na home é necessário acessar a *Home* >> *No componente Foto Agência* >> *Clicar na engrenagem no canto superior direito do componente*>> *Editar bloco*.
Em seguida o sistema exibe a tela abaixo:

image:img/editar-colecao-fotos-home.png[image,width=765,height=165] +

Ao digitar no *“Título da Coleção de foto”* e nos campos *“Fotos”* preencher com os títulos da foto e clicar no botão *“Salvar”*, é associado a(s) fotos cadastradas na página de fotos, exibindo a na home uma galeria como uma coleção de fotos de no máximo até 10 fotos.

== 3.7 Biblioteca de Mídia (Serve para os itens Conteúdo, Destaques, Destaque Carrosel Fotos, Pagina de Fotos, Página Básica) (Repórter, Editor, Editor Chefe)

Inserir imagem na Biblioteca

image:img/inserir-imagem-biblioteca.png[image,width=764,height=387]

Para abrir a biblioteca clique no primeiro ícone. Ao abrir a biblioteca será mostrado todos os arquivos de vídeo e imagens inseridos nela;

image:img/exibe-biblioteca.jpg[image,width=165,height=325]

Para inserir a imagem na biblioteca clique no ícone referente a nova imagem;

image:img/icone-biblioteca.gif[image,width=50,height=52]

Clique em _“Selecionar_ _arquivos”,_ o sistema exibe uma janela para a escolha dos arquivos salvos no seu computador.

image:img/upload-arquivo-biblioteca.png[image,width=764,height=387]

*Inserir imagem no post*

Para inserir uma imagem é necessário que a mesma já esteja na biblioteca _(arquivo de imagens, _*_Ver item 3._**_8_*_)_.

image:img/inserir-imagem-materia.jpg[image,width=765,height=390]

Abra a biblioteca, posicione o mouse sobre a imagem, clique e segure com o botão direito do mouse e arraste-a para a área “Imagem de Destaque” e solte.

*Inserir Vídeo na Biblioteca*

Para inserir um Vídeo na biblioteca acesse *Menu >> Criar conteúdo >> Biblioteca.* _(Este último fica na lateral direita da tela)_.

image:img/inserir-video-biblioteca.png[image,width=765,height=388] +

Para abrir a biblioteca clique no primeiro ícone. Ao abrir a biblioteca será mostrado todos os arquivos de Arquivo,Galeria de Imagens, Vídeo,Imagens e áudio inseridos nela;

image:img/exibe-biblioteca-video.png[image,width=573,height=325]

Para inserir o vídeo na biblioteca clique no ícone referente ao novo Vídeo;

image:img/icone-video.gif[image,width=50,height=52]

O sistema exibe uma janela para o usuário escolher qual o tipo de caminho do vinculo do vídeo (Upload de arquivo, Vimeo ou Youtube),conforme imagem abaixo.

image:img/tipo-upload-video.png[image,width=765,height=386]

Caso o Usuário escolha Upload Arquivo de Vídeo. Copio a url do youtube e colo no campo texto conforme mostrado na imagem abaixo, em seguida o usuário clica no botão “*Continuar*”. *Obs*: O mesmo processo feito para o youtube e igual para o vimeo.

image:img/inserir-url-video.png[image,width=765,height=330]

Em seguida o sistema exibe a tela para preenchimento dos campos relativos ao vídeo, conforme mostra a imagem abaixo:

image:img/inserir-dados-video.png[image,width=765,height=390] +

Você deverá informar os campos _“Título” (preenchimento obrigatório”,_ _“Autores” e “Tags”_, se deixado em branco, será usado o valor do campo padrão.

.. *Título:* Título do vídeo;

.. *Thumbnail:* Imagem que será exibido na biblioteca de mídia, caso o usuário deseje o sistema tem a opção de modificar clicando no botão “Remover”.O sistema automaticamente já pega o primeiro frame do vídeo como padrão;

.. *Direitos autorais:* Nome do autor do vídeo;

.. *Direitos de exibição:* Nome do autor do vídeo;

.. *Autores:* Nome do autor do vídeo;

.. *Tags:* Lista de palavras relacionadas ao conteúdo da página, separada por vírgula.Usar palavras que agregam em relação a outros conteúdos do mesmo site;

Clique no botão “*Finalizar*” para salvar o Vídeo na biblioteca.

*Inserir Vídeo no post*

Para inserir um Vídeo é necessário que o mesmo já esteja na biblioteca _(arquivo de_ _Vídeo,_ _**Ver item 3.****8***.2*)_.

image:img/inserir-video-materia.jpg[image,width=765,height=446] +

Abra a biblioteca, posicione o mouse sobre o Vídeo, clique e segure com o botão direito do mouse e arraste-o para a área _“Vídeo”_ e solte.

*Pesquisar Imagem, Vídeo, Áudio, Galeria ou Arquivo na Biblioteca*

image:img/icone-busca-biblioteca.png[image,width=44,height=29]

image:img/tela-busca-biblioteca.jpg[image,width=764,height=389]

.. *Editor:* Nome do usuário que inseriu o arquivo;

.. *Autores:* Nome do fotógrafo ou autor da imagem ou vídeo;

.. *Tipo:* Imagem ou vídeo;

.. *Tags:* Palavras relacionadas ao conteúdo da página;

.. *Ordenar por:* Tipo de ordenação a ser apresentada;

.. *Ordem:* Crescente ou ascendente;

Clicar em *“Aplicar”* e o sistema retornará com o resultado da pesquisa.

== 3.8 Administrar usuário (Apenas para o perfil “Administrador de Usuário” e Editor Chefe)

*Pesquisar usuário*

image:img/pesquisa-usuario.jpg[image,width=765,height=390] Para pesquisar um usuário é necessário acessar **Menu >> Pessoas**. Em seguida a tela abaixo será exibida.

Serão apresentados os seguintes campos:

.. *Nome:* login do usuário e e-mail do usuário cadastrado no sistema; **

.. *Status:* Usuário ativo ou bloqueado no sistema;

.. *Papel dos usuários:* São os perfis ao qual o usuário pertence;

.. *Membro há:* Quanto tempo esse usuário foi cadastrado no sistema;

.. *Último acesso:* Tempo em dias ou horas do último acesso do usuário no sistema;

.. *Operações:* Editar e Cancelar conta;

Após inserir os filtros conforme necessidade, clicar no botão “*Filtrar*” e o sistema retornará apresentando uma lista somente com os usuários relacionados ao filtro utilizado.

*Cadastrar usuário*

Para cadastrar um usuário é necessário acessar *Menu >> Pessoas>> Adicionar Usuário*.

image:img/adicionar-usuario.png[image,width=765,height=551] Em seguida clicar em *“**Adicionar Usuário”* a tela abaixo será exibida.

Os campos marcados com “*” deverão ser informados obrigatoriamente e opcionalmente os outros campos.

Serão apresentados os seguintes campos:

.. *Usuário:* Nome do usuário da rede;

.. *CAS username:* Nome do usuário da rede

.. *Endereço de e-mail:* Endereço do e-mail corporativo;

.. *Senha:* Senha do usuário;

.. *Confirme a senha:* Confirmação da senha;

.. *Status:* Ativo ou Bloqueado – caso opte por bloqueado o usuário não terá acesso ao sistema;

.. *Papéis:* O que será atribuído ao usuário. (Editor, Fotógrafo, Editor Chefe);

.. *Permissão de idioma:* Seleciona qual ou quais os idiomas que o usuário pode criar ou editar conteúdo - ** Padrão português (Inglês, Português e Espanhol);

.. *Configurações de idioma:* Padrão português;

.. *Nome Completo**:* Informar o nome completo do usuário.

Após informar os campos, clicar em “*Criar nova conta*”.

*Editar usuário*

image:img/pesquisa-usuario.jpg[image,width=765,height=390] Para editar os dados de um usuário acesse *Menu >> Pessoas*.

Caso necessário, utilize o filtro para encontrar o usuário *_(Veja item 4.1)_*.

Após encontrar o usuário, clique no link “*Editar*”. A tela abaixo será exibida.

Faça as alterações necessárias e clicar em “*Salvar*”. O sistema salva as alterações e retorna para a tela com a lista de usuários.

*Excluir usuário*

Refaça o passo anterior para editar o usuário.

No final da tela clique no botão “*Cancelar Conta”*

Uma tela de cancelamento será exibida para que você escolha a melhor opção.

image:img/cancela-usuario.png[image,width=765,height=231] Após marcar uma opção clique no botão “**Cancelar Conta**”. A conta será cancelada e a tela retornará para a lista de usuários.

== 3.9 Home (Capa)
=== Breaking News

Para incluir ou Alterar os dados do componente Breaking News um usuário deve clicar na “**engrenagem no canto superior direito do componente**” e clicar no menu “**Editar bloco**”. Em seguida a tela abaixo será exibida.

image:img/break-news.png[image,width=711,height=197] +

Serão apresentados os seguintes campos:

.. *Título:* Campo descrição livre com o máximo de 70 caracteres;

.. *Link:* Campo livre para o usuário informar o link da matéria que deseja destacar;

Exemplo: O usuário deseja destacar a matéria abaixo:

*cultura/noticia/2017-08/numero-de-visitantes-no-mercado-modelo-em- salvador-duplica-em-um-ano*

Após o preenchimento dos campos o usuário deve clicar no botão “**Salvar**”.
Para **Remover** o componente da capa, deixa o “**TÍTULO**” em branco e depois salve o fomulário.

=== Destaque Principal

Para incluir ou Alterar os dados do componente Destaque Principal o usuário deve clicar na “**engrenagem no canto superior direito do componente**” e clicar no menu “**Editar bloco**”.
Em seguida a tela abaixo será exibida.

image:img/destaque-principal-2019.png[image,width=765,height=424] Os campos marcados com “*” deverão ser informados obrigatoriamente e opcionalmente os outros campos.

Serão apresentados os seguintes campos:

.. *Título:* Campo descrição livre com o máximo de 70 caracteres;

.. *Chapéu:** Campo descrição livre com o máximo de 70 caracteres;

.. *Linha Fina:** Campo descrição livre com o máximo de 70 caracteres;

.. *Editoria**: Lista com as editorias a qual o conteúdo pertence (Economia,Educação,Geral,Internacional,Política,Saúde);

.. *Link:* Campo livre para o usuário informar o link da matéria que deseja destacar;

Exemplo: O usuário deseja destacar a matéria abaixo:

*cultura/noticia/2017-08/numero-de-visitantes-no-mercado-modelo-em- salvador-duplica-em-um-ano*

.. *Conteúdo Relacionado:**Permitido incluir até 3 conteúdo relacionado(somente permite relacionar matérias da Agência Brasil);

Após o preenchimento dos campos o usuário deve clicar no botão “**Salvar**”.

Para *Remover* o componente da capa, deixa o “*TÍTULO*” em branco e depois salve o fomulário.

=== Destaque Secundário (até 3 destaques)

Para incluir ou Alterar os dados do componente Destaque Secundário (12) o usuário deve clicar na “**engrenagem no canto superior direito do componente**” e clicar no menu “**Editar bloco**”. Em seguida a tela abaixo será exibida.

image:img/destaque-secundario.png[image,width=765,height=534] Os campos marcados com “*” deverão ser informados obrigatoriamente e opcionalmente os outros campos.


Serão apresentados os seguintes campos:

.. *Título:* Campo descrição livre com o máximo de 70 caracteres;

.. *Chapéu:** Campo descrição livre com o máximo de 70 caracteres;

.. *Editoria**: Lista com as editorias a qual o conteúdo pertence (Economia,Educação,Geral,Internacional,Política,Saúde);

.. *Link:* Campo livre para o usuário informar o link da matéria que deseja destacar;

Exemplo: O usuário deseja destacar a matéria abaixo:

**cultura/noticia/2017-08/numero-de-visitantes-no-mercado-modelo-em- salvador-duplica-em-um-ano**

.. *Indicativo de Mídia**: O usuário informa se a matéria possui mídia (Áudio,Vídeo ou Nenhuma);

.. *Tipo de Apresentação:* Permiti o usuário escolher até 4 layouts de visualização diferentes para o destaque;
... Sem foto - Exibe o título (link) e chapéu e linha fina e sem foto.
... foto thumb - Exibe a foto, editoria, título (link), chapéu e linha fina.
... foto de fundo - Exibe a foto como um fundo, editoria, título (link), chapéu, linha fina, esses últimos 3 itens será exibido na parte de cima do destaque.
... foto de fundo invertido - Exibe a foto como um fundo, editoria, título(link), chapéu, linha fina, esses últimos 3 itens será exibido na parte de baixo do destaque.

Após o preenchimento dos campos o usuário deve clicar no botão “**Salvar**”.

Para **Remover** o componente da capa, deixa o “**TÍTULO**” em branco e depois salve o formulário.

=== Destaque Fique de Olho (Até 4 destaques)

Para incluir ou Alterar os dados do componente Destaque Fique de Olho(4) o usuário deve clicar na “**engrenagem no canto superior direito do componente**” e clicar no menu “**Editar bloco**”. Em seguida a tela abaixo será exibida.

image:img/fique-de-olho.png[image,width=765,height=303] +

Os campos marcados com “*” deverão ser informados obrigatoriamente e opcionalmente os outros campos.

Serão apresentados os seguintes campos:

.. *Título:* Campo descrição livre com o máximo de 70 caracteres;

.. *Chapéu:* Campo descrição livre com o máximo de 70 caracteres;

.. *Editoria:* Lista com as editorias a qual o conteúdo pertence (Economia,Educação,Geral,Internacional,Política,Saúde);

.. *Link:* Campo livre para o usuário informar o link da matéria que deseja destacar;

Exemplo: O usuário deseja destacar a matéria abaixo:

**cultura/noticia/2017-08/numero-de-visitantes-no-mercado-modelo-em- salvador-duplica-em-um-ano**


Após o preenchimento dos campos o usuário deve clicar no botão “**Salvar**”.

Para **Remover** o componente da capa, deixa o “**TÍTULO**” em branco e depois salve o formulário.

=== Destaque Especial

Para incluir ou Alterar os dados do componente Destaque Especial o usuário deve clicar na “**engrenagem no canto superior direito do componente**” e clicar no menu “**Editar bloco**”. Em seguida a tela abaixo será exibida.

image:img/especiais.png[image,width=765,height=269] +


Os campos marcados com “*” deverão ser informados obrigatoriamente e opcionalmente os outros campos.

Serão apresentados os seguintes campos:

.. *Título:* Campo descrição livre com o máximo de 70 caracteres;

.. *Link:* Campo livre para o usuário informar o link da matéria que deseja destacar;

Exemplo: O usuário deseja destacar a matéria abaixo:

*cultura/noticia/2017-08/numero-de-visitantes-no-mercado-modelo-em- salvador-duplica-em-um-ano*

.. *Imagem:* Deve ser arrastada da biblioteca de Mídias (imagem). Caso não seja selecionada, nenhuma imagem será apresentada; Para remover a imagem do formulário , o usuário deve clicar no botão “**Remover**”, que fica abaixo da imagem.

Após o preenchimento dos campos o usuário deve clicar no botão “**Salvar**”.

Para *Remover* o componente da capa, deixa o “*TÍTULO*” em branco e depois salve o fomulário.
