Para o artefato Ata de Reuni�o deve ser inclu�do no nome a data de elabora��o e a sigla da fase em que foi elaborada, no seguinte padr�o:
Ata_de_Reuni�o_XX_XX_XXXX_Sigla
|- https://drive.google.com/open?id=1HI1rSg3a9tKF3Svn_49RvWuoHqJGX1wxwZWB3L0BzV0

As fases dos projetos podem ser identificadas por siglas:
Concep��o do Neg�cio - CN;
Levantamento de Requisitos - LR;
Constru��o - CT;
Implanta��o - IM;
Sustenta��o - ST;
Servi�os de Apoio - SA.

