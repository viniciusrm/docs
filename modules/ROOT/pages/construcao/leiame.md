Nessa Fase do projeto dever� conter os seguintes artefatos:
Poder� ser dividido em sprints.
Sprint1,Sprint2,Sprint3...Sprintn
Para cada sprint dever� conter os documentos abaixo relacionados.

- Lista de Atividades( Sprint Backlog)
 |- https://drive.google.com/open?id=17uC0acZxR-Yov8HfdyXk2vKRQ4Ti6F52wzEhwsuUpxg

- Checklist de Teste
 |- https://drive.google.com/open?id=1Dys-7RC7srpimibh4ie3glLlKNihN1fflzp6VzaQogM

- Plano de Melhorias
 |- https://drive.google.com/open?id=1jDyMmJEKK1znsjrexRE29yKi8pgjSUAWiUwpyCXmkJc

- C�digo-Fonte

- Script de Banco de Dados

- Documento de Arquitetura de Software
|- https://drive.google.com/open?id=140B2LSHGcXo2xZp3qjnhqggLtx9zzH66xiRh8fYx1xQ